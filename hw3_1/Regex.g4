grammar Regex;

re
    : section EOF
    ;

section
    : expression
    | alt
    ;

alt
    : expression '|' expression
    | expression '|' section
    ;

expression
    : (elems+=element)*
    ;

element
    : a=atom            # AtomicElement
    | a=atom m=MODIFIER # ModifiedElement
    ;

atom
    : c=CHARACTER # Atomic
    | '(' section ')' # Paren
    ;

CHARACTER : [a-zA-Z0-9.] ;
MODIFIER : [?*] ;
WS : [ \t\r\n]+ -> skip ;
